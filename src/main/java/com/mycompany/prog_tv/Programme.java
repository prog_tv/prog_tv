

package com.mycompany.prog_tv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Programme {
    
    private static final Logger logger = LoggerFactory.getLogger(Programme.class);
    protected String nomProg;
    protected int dureeProg;
    protected int heureDebut;
    
    public int heureFin(int heureDebut, int duree){
        logger.info(">>> heureFin heureDebut: {}, duree: {}", heureDebut, duree);
        logger.info("<<< ToString() return type: String {}", heureDebut + duree);
        return(heureDebut + duree);
    }

    public Programme(String nomProg, int dureeProg, int heureDebut){
        logger.info(">>> heureFin nomProg: {}, heureDebut: {}, duree: {}", nomProg, dureeProg, heureDebut); 
        this.nomProg=nomProg;
        this.dureeProg=dureeProg;
        this.heureDebut=heureDebut;
    }
    
    public Programme(){
        logger.info(">>> Programme"); 
        this.nomProg = "Unknown";
        this.dureeProg = 0;
        this.heureDebut = 24;
    }

    /**
     * @return the dureeProg
     */
    public int getDureeProg() {
        return dureeProg;
    }

    /**
     * @param nomProg the nomProg to set
     */
    public void setNomProg(String nomProg) {
        this.nomProg = nomProg;
    }

    /**
     * @return the nomProg
     */
    public String getNomProg() {
        return nomProg;
    }
    
    public String ToString(){
        return "";
    }

    /**
     * @return the heureDebut
     */
    public int getHeureDebut() {
        return heureDebut;
    }
    
   
}

