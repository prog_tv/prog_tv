package com.mycompany.prog_tv;

import java.util.ArrayList;

public class Application
{
    public static void main( String[] args )
    {        
        
        // test 1
        
        ArrayList<String> programmeList = new ArrayList<>();

        programmeList.add("fiction, 20, 2, A.I., 2001, Steven Spielberg, OUI");

        programmeList.add("reportage, 0, 1, Les animaux c'est cool, animalier");

        programmeList.add("rEpoRtage, 2, 1, Télé matin, information");

        programmeList.add("fiction, 10, 2, A.I., 2001, Steven Spielberg, oui");
        
        programmeList.add("reportage, 1, 1, Des racines et des ailes, culturel");
        
        programmeList.add("divertISSEMENT, 18, La roue de la fortune, machin");
        
        programmeList.add("fiction, 22, 2, Il faut sauver les soldat Ryan, 1998, Steven Spielberg, oui");
        
        programmeList.add("reportage, 3, 1, Thalassa, cultuREL");
        
        programmeList.add("fiction, 4, 2, Charlie et la chocolaterie, 2005, Tim Burton, oui");
        
        programmeList.add("fiction, 6, 2, Le cinquième élément, 1994, Luc Besson, oui");
        
        programmeList.add("fiction, 8, 2, La reine des neiges, 2014, Disney, oui");
        
        programmeList.add("fiction, 12, 2, Le roi lion, 1994, Disney, oui");
        
        programmeList.add("reportage, 14, 1, Journal, information");
        
        programmeList.add("reportage, 15, 1, C dans l'air, information");
        
        programmeList.add("reportage, 16, 1, L'économie Chinoise, information");
        
        programmeList.add("reportage, 17, 1, Envoyé spécial, information");

        ProgTele p1 = new ProgTele(programmeList);
        System.out.println(p1.HeureParHeure());
        System.out.println(p1.ToString());
        p1.VerifSupperposition();
        
        //test 2
        
        ArrayList<String> programmeList2 = new ArrayList<>();
        
        programmeList2.add("fiction, 0, 24, Guerre et paix, 1967,  Serge Bondartchouk, oui");
        
        ProgTele p2 = new ProgTele(programmeList2);
        System.out.println(p2.HeureParHeure());
        System.out.println(p2.ToString());
        p2.VerifSupperposition();
        
        
        //test constructeur vide
        
        ProgTele p3 = new ProgTele();
        System.out.println(p3.HeureParHeure());
        System.out.println(p3.ToString());
        
        //test supperposition
        
        ArrayList<String> programmeList3 = new ArrayList<>();
        
        programmeList3.add("fiction, 21, 2, test1, 000, mec, non");
        
        programmeList3.add("fiction, 21, 2, test2, 000, mec, non");
        
        programmeList3.add("fiction, 0, 2, test3, 000, mec, oui");
        
        programmeList3.add("fiction, 1, 2, test4, 000, mec, oui");
        
        ProgTele p4 = new ProgTele(programmeList3);
        System.out.println(p4.HeureParHeure());
        System.out.println(p4.ToString());
        p4.VerifSupperposition();
    }
}