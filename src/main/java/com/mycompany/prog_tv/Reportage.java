    
package com.mycompany.prog_tv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Reportage extends Programme {
    
    private static final Logger logger = LoggerFactory.getLogger(Reportage.class);
    private String theme;
    
    public Reportage(String nomProg, int dureeProg, int heureDebut, String theme){
      super(nomProg, dureeProg, heureDebut);
      logger.info(">>> Reportage nomProg: {}, dureeProg: {}, heureDebut: {}, theme: {}", nomProg, dureeProg, heureDebut, theme); 
      this.theme = theme.toLowerCase();
      
      if ((6<heureDebut)&&(heureDebut<14)||(heureDebut>17)||(dureeProg!=1) || ((this.theme.equalsIgnoreCase("information")) && (this.theme.equalsIgnoreCase("animalier")) && (this.theme.equalsIgnoreCase("culturel")))){
          
          if((6<heureDebut)&&(heureDebut<14)||(heureDebut>17))
            System.out.println("Veuillez programmer "+nomProg+" à une autre heure !\r\n");
          
          if(dureeProg!=1)
            System.out.println("Veuillez changer la durée du programme "+nomProg+"\r\n");
          
          if((this.theme.equalsIgnoreCase("information")) && (this.theme.equalsIgnoreCase("animalier")) && (this.theme.equalsIgnoreCase("culturel")))
            System.out.println("Le thème de "+nomProg+" n'est pas reconnu !"+this.theme+"\r\n");
          
          this.heureDebut=24;
          this.dureeProg=0;
        }
    }
     
    public Reportage(){
        super();
        logger.info(">>> Reportage");
        this.theme="Unknown";       
    }
    public String ToString(){
        logger.info(">>> ToString");
        logger.info("<<< ToString retour OK");
        return ("vous pourrez regarder "+nomProg+", un reportage sur le theme : "+theme+" d'une durée de "+dureeProg+"h.");
    }
}
