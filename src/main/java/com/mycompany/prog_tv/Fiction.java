

package com.mycompany.prog_tv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Fiction extends Programme {
    
    private static final Logger logger = LoggerFactory.getLogger(Fiction.class);
    private int anneeReal;
    private String nomReal;
    private boolean rediffusion;
    
    public Fiction(String nomProg, int dureeProg, int heureDebut, int anneeReal, String nomReal, boolean rediffusion){
        super(nomProg, dureeProg, heureDebut);
        logger.info(">>> Fiction nomProg: {}, dureeProg: {}, heureDebut {}, anneeReal {}, nomReal {}, rediffusion {}", nomProg, dureeProg, heureDebut, anneeReal, nomReal, rediffusion);
        this.anneeReal = anneeReal;
        this.nomReal = nomReal;
        this.rediffusion = rediffusion;
        if(rediffusion==false){
           if(heureDebut == 21 ) 
            this.heureDebut=heureDebut;
           else
           {
            this.heureDebut=24;
            this.dureeProg=0;
            System.out.println("Veuillez programmer "+nomProg+" à une autre heure !\r\n");
           } 
        }
    }
    
    public Fiction(){
        super();
        logger.info(">>> Fiction");
        this.anneeReal=0;
        this.nomReal="Unknown";
        this.rediffusion=false;
    }
    
    public String ToString(){
        logger.info(">>> ToString");
        String redif="non";
        if (rediffusion){
            logger.info("<<< ToString retour OK");
            return("vous pourrez regarder "+nomProg+", une fiction réalisée par"+nomReal+" en "+anneeReal+" d'une durée de "+dureeProg+"h.");
        }
        else{
            logger.info("<<< ToString retour OK");
            return("vous pourrez regarder en exclusivité "+nomProg+", une fiction réalisée par"+nomReal+" en "+anneeReal+" d'une durée de "+dureeProg+"h.");
        }
    }
}