/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.prog_tv;

import java.util.ArrayList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author quentin.sutkowski
 */
public class ProgTele {
    
    
    private static final Logger logger = LoggerFactory.getLogger(ProgTele.class);
    
    private Programme tabProg[] = new Programme[25];
    private String[] parts = null;
    private int supper = 0;
       
    
    public ProgTele(ArrayList<String> programmeList){
       
        logger.info(">>> ProgTele programmeList: {}", programmeList);
        for(int i=0;i<24;i++)
            tabProg[i] = new Programme();
        
        for(int i=0;i<programmeList.size();i++){
            parts = programmeList.get(i).split(", ");
            switch (parts[0].toLowerCase()) {
                case "reportage" :
                { 
                    if(tabProg[Integer.parseInt(parts[1])].getNomProg() != "Unknown")
                        supper=supper+1;
                    tabProg[Integer.parseInt(parts[1])] = new Reportage(parts[3],Integer.parseInt(parts[2]),Integer.parseInt(parts[1]),parts[4]);
                    break;
                }    
                case "divertissement" :
                {    
                    if(tabProg[Integer.parseInt(parts[1])].getNomProg() != "Unknown")
                        supper=supper+1;
                    tabProg[Integer.parseInt(parts[1])] = new Divertissement(parts[2],Integer.parseInt(parts[1]),parts[3]);
                    break;
                }    
                case "fiction" :
                {
                    if(tabProg[Integer.parseInt(parts[1])].getNomProg() != "Unknown")
                        supper=supper+1;
                    if(parts[6].toLowerCase().equalsIgnoreCase("oui"))
                        tabProg[Integer.parseInt(parts[1])] = new Fiction(parts[3],Integer.parseInt(parts[2]),Integer.parseInt(parts[1]),Integer.parseInt(parts[4]),parts[5],true);
                    else if(parts[6].toLowerCase().equalsIgnoreCase("non"))
                        tabProg[Integer.parseInt(parts[1])] = new Fiction(parts[3],Integer.parseInt(parts[2]),Integer.parseInt(parts[1]),Integer.parseInt(parts[4]),parts[5],false);
                    break;
                }    
            }
        }
        for(int i=0;i<24;i++){
            if(tabProg[tabProg[i].getHeureDebut()] != tabProg[i]){
                tabProg[i] = new Programme();
            }
        }
        for(int i=0;i<24;i++)
            if((tabProg[i].heureFin(tabProg[i].getHeureDebut(), tabProg[i].getDureeProg())>=24) && (tabProg[i].getDureeProg() != 0))
                System.out.println("!! ATTENTION !! votre programmation ne rentre pas dans la grille de programme\r\n\r\n");
    }
    public ProgTele(){
        logger.info(">>> ProgTele");
        for(int i=0;i<=23;i++)
            tabProg[i] = new Programme();
    }
    
    
    
    
    public String HeureParHeure(){
        logger.info(">>> HeureParHeure");
        String chaine = "-- AFFICHAGE HEURE PAR HEURE --\r\n\r\n";
        boolean flag = false;
        for(int i=0;i<24;i++){
            if(tabProg[i].getNomProg() != "Unknown"){
                chaine = chaine +"A "+ i + "h " + tabProg[i].ToString() + "\r\n";
                for(int y=1;y<tabProg[i].getDureeProg();y++)
                    chaine = chaine +"A "+  (i+y) + "h: Programme en cours...\r\n";
                i=i+tabProg[i].getDureeProg()-1;    
            }
            else
            {    
                chaine = chaine + "A "+ i + "h: (Vide)\r\n";
                flag = true;
            }    
        }
        if(flag==true)
            chaine = chaine + "\r\n!!! Le programme contient des plages horaires vides et doit donc être complété !!!\r\n";
    logger.info("<<< HeureParHeure() return type: String {}", chaine);    
    return (chaine);        
    }
    
    
    
    
    public String ToString(){
        logger.info(">>> ToString");
        String chaine ="-- LISTE DES EMISSIONS --\r\n\r\n";
        for(int i=0;i<24;i++){
            if(tabProg[i].getNomProg()!="Unknown")
                chaine = chaine + tabProg[i].ToString()+ "\r\n";
        }
        logger.info("<<< ToString() return type: String {}", chaine);
        return(chaine);
    }
    
    
    
    public void VerifSupperposition(){
        logger.info(">>> VerifSupperposition");
        for(int i=0;i<24;i++)
        {
            if(tabProg[i].getDureeProg() != 0){
                for(int y=1;y<tabProg[i].getDureeProg();y++)
                    if(tabProg[i+y].getNomProg() != "Unknown")
                        supper = supper + 1;
            }                
        }
        if(supper==0)
            System.out.println("Il n'y a aucune supperposition dans le programme !\r\n");
        else
            System.out.println("Il y a "+supper+" supperpositions dans votre programme, attention !!! \r\n");
    }    
}

