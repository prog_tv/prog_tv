

package com.mycompany.prog_tv;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
public class Divertissement extends Programme{
    
    private static final Logger logger = LoggerFactory.getLogger(Divertissement.class);
    private String animateur;
    
    public Divertissement(String nomProg, int heureDebut, String animateur){
      logger.info(">>> Divertissement nomProg: {}, heureDebut: {}, animateur: {}", nomProg, heureDebut, animateur);  
      this.animateur = animateur;
      this.nomProg = nomProg;
      this.dureeProg = 2;
      if((heureDebut<18) || (heureDebut>21))
      {
          this.heureDebut = 24;  //hors de la grille de programmation de 24h
          this.dureeProg = 0;
          System.out.println("Veuillez programmer "+nomProg+" à une autre heure !\r\n");
      }
      else
      {
          this.heureDebut = heureDebut;
      }
    }
   public Divertissement(){ 
      super();
      logger.info(">>> Divertissement");
      this.animateur = "Unknown";
   } 
   
   public String ToString(){
        logger.info(">>> ToString");
        logger.info("<<< ToString retour OK");
        return ("vous pourrez regarder "+nomProg+", un divertissement présenté par "+animateur+".");
    }
}
