# README #

**But du projet :**

Optimisation de la programmation d'une chaine TV :

3 types de prog : - Divertissement
                  - Fiction
                  - Reportage
                  
Contraintes : - Divertissement : durée 2h, programmable entre 18 et 23h.
              - Fiction : si inédit, alors programmable qu'à 21h, sinon pas de contrainte particulière.
              - Reportage : programmables qu'en heures creuses (18h-24h ou 0h-6h), et durée inférieure ou égale à 1h (donc forcemment 1heure).
 
**INFORMATIONS**

La quasi totalité du projet a été réalisé à deux, le nom sur les commits est donc indicatif et relativement aléatoire.
Pour ce qui est des pseudos des commits RomainGoutiere = Supercaty
                            
 